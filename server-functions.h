
#include "server-functions-adds.h"

void caz1(char msgRasp[bufferSize], char path[size])
{
    // char msgRasp[bufferSize];
    // printf("%s \n", path);
    struct arb *root = malloc(sizeof(*root));
    struct arb *coada[200];
    int indC = 0;
    char buffer[bufferSize];
    readfile(buffer, path);
    parse(root, coada, &indC, buffer);
    // scanf("%s", buffer);

    struct arb *coadaParc[200];
    struct arb *x;

    int index = 0;
    int len = root->childCount;

    for (int i = 0; i < len; i++)
        coadaParc[i] = root->child[i];
    printf("\n\n");
    for (int i = 0; i < root->childCount; i++)
    {
        int lenTrain = coadaParc[i]->childCount;
        for (int j = 0; j < lenTrain; j++)
        {
            x = coadaParc[i]->child[j];
            addToPrint(msgRasp, x);
        }
        // estimare_sosrie(msgRasp, )
        addEstimare(root->child[i], msgRasp);
        strcat(msgRasp, "\n\n");
    }
    // printf("%s \n", msgRasp);
}
void caz2(char msgRasp[bufferSize], char path[size])
{
    struct timeStruct actual;
    initTimeStruct(&actual);

    struct arb *root = malloc(sizeof(*root));
    struct arb *coada[200];
    int indC = 0;
    char buffer[bufferSize];
    readfile(buffer, path);
    parse(root, coada, &indC, buffer);
    // scanf("%s", buffer);

    struct arb *coadaParc[200];
    struct arb *x;

    int index = 0;
    int len = root->childCount;

    // memset(msgRasp, '\0', 2000);

    for (int i = 0; i < len; i++)
        coadaParc[i] = root->child[i];
    // printf("\n\n");

    for (int i = 0; i < len; i++)
    {
        x = coadaParc[i]->child[2];
        struct timeStruct trainProgram;

        getTimeFromChar(x, &trainProgram);
        // printf("%d %d \n", trainProgram.h, trainProgram.m);
        // printf("%d %d \n", actual.h, actual.m);
        if ((trainProgram.h == actual.h && trainProgram.m <= actual.m) || (trainProgram.h == actual.h - 1 && trainProgram.m >= actual.m))
        {
            int lenTrain = coadaParc[i]->childCount;
            for (int j = 0; j < lenTrain; j++)
            {
                x = coadaParc[i]->child[j];
                addToPrint(msgRasp, x);
            }
            addEstimare(coadaParc[i], msgRasp);
        }
    }
    strcat(msgRasp, "\n");
}
void caz3(char msgRasp[bufferSize], char path[size])
{

    struct timeStruct actual;
    initTimeStruct(&actual);

    struct arb *root = malloc(sizeof(*root));
    struct arb *coada[200];
    int indC = 0;
    char buffer[bufferSize];
    readfile(buffer, path);
    parse(root, coada, &indC, buffer);

    struct arb *coadaParc[200];
    struct arb *x;

    int index = 0;
    int len = root->childCount;

    memset(msgRasp, '\0', 2000);

    for (int i = 0; i < len; i++)
        coadaParc[i] = root->child[i];

    for (int i = 0; i < len; i++)
    {
        x = coadaParc[i]->child[3];
        struct timeStruct trainProgram;

        getTimeFromChar(x, &trainProgram);
        // printf("%d %d \n", trainProgram.h, trainProgram.m);
        // printf("%d %d \n\n\n", actual.h, actual.m);
        if ((trainProgram.h == actual.h && trainProgram.m <= actual.m) || (trainProgram.h == actual.h - 1 && trainProgram.m >= actual.m))
        {
            test;
            int lenTrain = coadaParc[i]->childCount;
            for (int j = 0; j < lenTrain; j++)
            {
                x = coadaParc[i]->child[j];
                addToPrint(msgRasp, x);
            }
            // addEstimare(coadaParc[i], msgRasp);
        }
        // printf("%d %d \n", trainProgram.h, trainProgram.m);
    }
    // printf("%s \n", msgRasp);
}
void caz5(char msgRasp[bufferSize], char params[100], char path[size])
{
    // printf("%s \n", params);
    struct tren train;
    memset(&train, 0, sizeof(train));

    workInputForLate(&train, params);

    struct arb *root = malloc(sizeof(*root));
    struct arb *coada[200];
    int indC = 0;
    char buffer[bufferSize];
    readfile(buffer, path);
    parse(root, coada, &indC, buffer);
    // test;

    struct arb *coadaParc[200];
    struct arb *x;

    int index = 0;
    int len = root->childCount;

    memset(msgRasp, '\0', 2000);

    for (int i = 0; i < len; i++)
        checkNode(root->child[i], train, root, path);

    //pana aici am interpretat input ul
    strcpy(msgRasp, "done");
}
void main1(char msg[bufferSize], int nr, char params[bufferSize], int cl)
{
    memset(msg, '\0', bufferSize);
    char paramsSave[bufferSize];
    memset(paramsSave, '\0', bufferSize);
    strcpy(paramsSave, params);
    DIR *dir;
    struct dirent *ent;
    int count=0;
    if ((dir = opendir("./train_data")) != NULL)
    {
        while ((ent = readdir(dir)) != NULL)
        {
            if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0)
                continue;
            char path[size];
            memset(path, '\0', size);
            strcpy(path, "./train_data/");
            strcat(path, ent->d_name);
            printf("%s \n", path);
            switch (nr)
            {
            case 1:
                // test;
                count++;
                caz1(msg, path);
                if(count==2)
                if (write(cl, msg, strlen(msg)) <= 0)
                {
                    perror("Raspuns trimis\n");
                }
                break;
            case 2:
                caz2(msg, path);
                if (write(cl, msg, strlen(msg)) <= 0)
                {
                    perror("Raspuns trimis\n");
                }
                break;
            case 3:
                caz3(msg, path);
                if (write(cl, msg, strlen(msg)) <= 0)
                {
                    perror("Raspuns trimis\n");
                }
                break;
            case 4:
                strcpy(params, paramsSave);
                caz5(msg, params, path);
                // test;
                if (write(cl, msg, strlen(msg)) <= 0)
                {
                    perror("Raspuns trimis\n");
                }
                break;
            default:
                break;
            }
            // printf("%s \n", msg);
        }
        closedir(dir);
    }
    else
    {
        perror("");
    }
    // test;
}

void f_main(int cl, char msg[bufferSize], int nr)
{
    char params[size];
    switch (nr)
    {
    case 1:
        main1(msg, nr, "\0", cl);
        // main 1 este pentru cazurile 1,2,3
        //main 2 este pentru cazul 5
        break;
    case 2:
        main1(msg, nr, "\0", cl);
        break;
    case 3:
        main1(msg, nr, "\0", cl);
        break;
    case 4:
        read(cl, params, size);
        main1(msg, nr, params, cl);
        // caz5(msg, params);
        break;
    case 5:
        printf("Chat \n");
        char mesaje[size];
        memset(mesaje, '\0', size);
        while (1)
        {
            read(cl, mesaje, size);
            // printf("%s \n", mesaje);

            int fd;
            if ((fd = open("mesaje.txt", O_APPEND)) == -1)
            {
                perror("open for write");
                exit(1);
            }
            char old[size];
            read(fd, old, size);
            close(fd);
            if (fd = open("mesaje.txt", O_WRONLY)==-1)
            {
                perror("open for write");
                exit(1);
            }

            printf("%s \n", old);
            // strcat(old, "\n");
            strcat(old, mesaje);
                      
            write(fd, old, strlen(old));
            write(cl, old, strlen(old));
            close(fd);
            sleep(2);
        }
        //
        // if (fcntl(fd, F_SETLKW, &fl) == -1)
        // {
        // perror("fcntl");
        // exit(1);
        // }
        // write(fd, mesaje, strlen(mesaje));
        // fl.l_type = F_UNLCK;
        //
        // if (fcntl(fd, F_SETLK, &fl) == -1)
        // {
        // perror("fcntl2");
        // exit(1);
        // }

        break;
    default:
        break;
    }
    // printf("%s \n", msg);
}
int security(char login[size])
{
  char username[size];
  char password[size];
  char *p = strstr(login, "\n");
  char ac;
  strncpy(username, login, p - login);
  strncpy(password, p, strlen(p));
  deleteWhiteSpaces(password);
  memset(login, '\0', size);
  strcpy(login, username);
  strcat(login, "--");
  strcat(login, password);
  strcat(login, "\n");
  FILE *fp;
  char buffer[255];

  fp = fopen("./users_data/users", "r");

  while (fgets(buffer, 255, (FILE *)fp))
  {
    // printf("%s \n", login);
    // printf("%s \n", buffer);
    if (strstr(login, buffer) != NULL && strstr(buffer, login) != NULL)
      return 1;
  }

  fclose(fp);
  return 0;
}
