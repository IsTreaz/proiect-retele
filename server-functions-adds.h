#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#include <dirent.h>
#include "parser.h"

void allLower(char string[bufferSize])
{
    int length = strlen(string);
    for (int i = 0; i < length; i++)
    {
        if (isupper(string[i]))
            string[i] = tolower(string[i]);
    }
}
void deleteWhiteSpaces(char string[bufferSize])
{
    int length = strlen(string);
    for (int i = 0; i < length; i++)
    {
        if (string[i] == ' ' || string[i] == '\n')
        {
            strcpy(string + i, string + i + 1);
            i--;
        }
    }
}
void trainNodeLower(struct arb *x)
{
    allLower(x->child[0]->content);
    allLower(x->child[1]->content);
    deleteWhiteSpaces(x->child[0]->content);
    deleteWhiteSpaces(x->child[1]->content);
    // deleteWhiteSpaces(x->child[2]->content);
}
void workInputForLate(struct tren *train, char params[size])
{
    int i = 0;
    char s[size];

    while (isalpha(params[i++]))
        ;
    strncpy(train->statieIn, params, i - 1);

    while (!isalpha(params[i++]))
        ;
    strcpy(params, params + i - 1);
    //fac Statie Out
    while (isalpha(params[i++]))
        ;
    strncpy(train->statieOut, params, i - 2);

    while (!isdigit(params[i++]))
        ;
    strcpy(params, params + i - 2);
    // printf("%s \n", train->statieOut);
    train->late = atoi(params);
    // printf("%d \n", train->late);
}
void getTimeFromChar(struct arb *x, struct timeStruct *date)
{
    char content[100];
    char hour[100], mins[100];

    memset(content, '\0', 100);

    strcpy(content, x->content);
    strncpy(hour, content, 2);
    strcpy(mins, content + 3);

    date->h = atoi(hour);
    date->m = atoi(mins);
}

void printTagContent(struct arb *x)
{
    char tag[100];
    memset(tag, '\0', 100);
    strncpy(tag, x->tag + 1, strlen(x->tag) - 2);
    // strncpy(tag,tag, strlen(tag)-2);
    printf("%s %s \n", tag, x->content);
}
void addToPrint(char msg[bufferSize], struct arb *x)
{
    // test;
    char tag[100];
    memset(tag, '\0', 100);
    strncpy(tag, x->tag + 1, strlen(x->tag) - 2);

    strcat(msg, tag);
    strcat(msg, "  ");
    strcat(msg, x->content);
    strcat(msg, "\n");
}
void addEstimare(struct arb *x, char msgRasp[bufferSize])
{
    // strcat(msgRasp, x->child[3]->tag);

    struct timeStruct data;
    getTimeFromChar(x->child[3], &data);
    int leaving;
    leaving= atoi (x->child[4]->content);
    data.m = data.m+leaving;
    data.h = data.m / 60 + data.h;
    data.m = data.m %60;

    char hour[size];
    char mins[size];

    memset(hour, '\0', size);
    memset(mins, '\0', size);
    if(data.m<10)
        {mins[1] = data.m + '0';
        mins[0] = '0';
        }
    else
        {
        mins[1] = data.m %10 + '0';
        mins[0] = data.m /10 + '0';
        }

    if(data.h <10)
    {
        hour[1] = data.h + '0';
        hour[0] = '0';
    }
    else
    {
        hour[1] = data.h %10 +'0';
        hour[0] = data.h /10 + '0';
    }
    
    strcat(msgRasp, "Estimare sosire: ");
    strcat(msgRasp, hour);
    strcat(msgRasp, " ");
    strcat(msgRasp, mins);
    strcat(msgRasp, "\n");  


    //xaxaxa

}
void initTimeStruct(struct timeStruct *x)
{
    struct tm *gtime;
    time_t now;

    time(&now);
    gtime = gmtime(&now);

    x->h = (gtime->tm_hour + 2) % 24;
    x->h = (x->h + 1) % 24;
    x->m = gtime->tm_min;
}
void WriteXML(struct arb *root, int fd)
{
    // printf("%s \n", root->tag);
    write(fd, "\n", 1);
    write(fd, root->tag, strlen(root->tag));
    if (root->childCount)
        for (int i = 0; i < root->childCount; i++)
            WriteXML(root->child[i], fd);
    else
    {
        write(fd, " ", 1);
        write(fd, root->content, strlen(root->content));
        write(fd, " ", 1);
        // write(fd, "\n", 1);
    }
    write(fd, root->end_tag, strlen(root->end_tag));
    write(fd, "\n", 1);
}
void lockFile(struct tren train, struct arb *root, char path[size])
{
    struct flock fl = {F_WRLCK, SEEK_SET, 0, 0, 0};
    struct flock fl2;
    int fd;
    if ((fd = open(path, O_WRONLY)) == -1)
    {
        perror("open for write");
        exit(1);
    }
    if (fcntl(fd, F_SETLKW, &fl) == -1)
    {
        perror("fcntl");
        exit(1);
    }
    WriteXML(root, fd);
    fl.l_type = F_UNLCK;

    if (fcntl(fd, F_SETLK, &fl) == -1)
    {
        perror("fcntl2");
        exit(1);
    }
    //unlocked
}
void itoa(int nr, char s[bufferSize])
{
    int x;
    int y;
    y = nr;
    memset(s, '\0', bufferSize);
    while (y)
    {
        x = y % 10;
        if (strlen(s) == 0)
        {
            s[0] = x + '0';
        }
        else
        {
            strcpy(s + 1, s);
            s[0] = x + '0';
        }
        y /= 10;
    }
}
void checkNode(struct arb *x, struct tren train, struct arb *root, char path[size])
{
    // x->child[0]->content
    struct timeStruct nodeTime;

    trainNodeLower(x);
    // printf("%s %s \n", train.statieIn, train.statieOut);
    // printf("%s %s \n\n", x->child[0]->content, x->child[1]->content);

    int ok = 1;
    if (!strstr(x->child[0]->content, train.statieIn) && !strstr(train.statieIn, x->child[0]->content))
        ok = 0;

    if (!strstr(x->child[1]->content, train.statieOut) && !strstr(train.statieOut, x->child[1]->content))
        ok = 0;
    // getTimeFromChar(x->child[2], &nodeTime);
    // if (train.leaving.h != nodeTime.h && train.leaving.m != nodeTime.m)
    // ok = 0;
    // test;
    if (ok == 1)
    {
        test;
        // printf("%s \n", x->child[0]->content);
        // printf("%s \n", x->child[1]->content);
        // test;
        // printf("%s \n", x->child[4]->content);
        // test;
        itoa(train.late, x->child[4]->content);
        lockFile(train, root, path);
    }
}