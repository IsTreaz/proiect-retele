#define tagSize 200
#define bufferSize 300000
#define size 100

struct arb{
    char tag[tagSize];
    char end_tag[tagSize];
    char content[bufferSize];
    int childCount;
    struct arb* child[200];
    struct arb* parent;
};

struct timeStruct{
    int h, m;
};

struct tren{
    char statieIn[size];
    char statieOut[size];
    struct timeStruct leaving;
    struct timeStruct arriving;
    int late;
};