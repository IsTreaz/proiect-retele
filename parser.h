#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ctype.h>
#include "declare.h"
#define true 1
#define false 0
#define test printf("xaxa \n")
#define tagSize 200
#define bufferSize 300000
#define size 100

void readfile(char buffer[bufferSize], char path[size])
{
    FILE *fp;
    char c[] = "this is tutorialspoint";

    fp = fopen(path, "r");
    // printf("Eror openning");
    // if(ferror(fp))
    //     perror("error");
    // if(feof(fp))
    //     perror("end of file");

    //    fseek(fp, 1,SEEK_SET);
    int n = fread(buffer, bufferSize, 1, fp);
    fclose(fp);
}
void getTag(char content[bufferSize], char tag[tagSize])
{
    memset(tag, '\0', 200);
    const char start[10] = "<";
    char *p;
    p = strstr(content, start);
    if (p != NULL)
    {
        int i = 0;
        int m = 0;
        while (*(p + i) != '>')
        {
            tag[m++] = p[i++];
        }
        tag[m++] = p[i];
    }
}
void getEndTag(char tag[tagSize], char endTag[tagSize])
{
    strcpy(endTag, "</");
    strcat(endTag, tag + 1);
}
void getContent(char buffer[bufferSize], char tag[tagSize], char endTag[tagSize], char content[bufferSize])
{
    const char start[10] = "<";
    char *p;
    p = (char *)malloc(bufferSize);
    p = strstr(buffer, start);
    char *p1;
    p1 = strstr(p, endTag);
    strncpy(content, p + strlen(tag) + 1, p1 - p - strlen(endTag) - 1);
    char TempBuffer[bufferSize];
    strcpy(TempBuffer, p1 + strlen(endTag));
    strcpy(buffer, TempBuffer);

    // printf("%s \n", buffer);
}
void initStruct(char buffer[bufferSize], struct arb *x)
{
    getTag(buffer, x->tag);
    getEndTag(x->tag, x->end_tag);
    getContent(buffer, x->tag, x->end_tag, x->content);
}
int avabileChilds(char content[bufferSize])
{
    char *p;
    const char sub[20] = "<";
    p = strstr(content, sub);
    int ok = 1;
    if (p == NULL)
        ok = 0;
    return ok;
}
void addOneChild(struct arb *root, struct arb *coada[200], char content[bufferSize], int *indC)
{
    struct arb *c = malloc(sizeof(*c));
    c = malloc(sizeof(*c));
    c->parent = root;
    root->child[root->childCount++] = c;
    initStruct(content, c);
    coada[*indC] = c;
    *indC = *indC + 1;
}
void parse(struct arb *root, struct arb *coada[200], int *indC, char buffer[bufferSize])
{
    initStruct(buffer, root);
    root->parent = NULL;
    root->childCount = 0;
    char content[bufferSize];

    strcpy(content, root->content);
    while (avabileChilds(content))
    {
        addOneChild(root, coada, content, indC);
        // printf("%s\n", coada[*indC-1]->content);
        // break;
    }
    int i = 0;
    while (i < *indC)
    {

        strcpy(content, coada[i]->content);
        while (avabileChilds(content))
        {
            addOneChild(coada[i], coada, content, indC);
            //  printf("%s \n", coada[*indC-1]->content);
        }
        i++;
    }
}

// struct arb *root = malloc(sizeof(*root));
// struct arb *coada[200];
// int indC = 0;
// char buffer[bufferSize];
// readfile(buffer);
// parse(root, coada, &indC, buffer);
// scanf("%s", buffer);
