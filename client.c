#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>

extern int errno;

int port;

int main(int argc, char *argv[])
{
  int sd;                    // descriptorul de socket
  struct sockaddr_in server; // structura folosita pentru conectare
                             // mesajul trimis
  int nr = 0;
  char buf[10];

  if (argc != 3)
  {
    printf("Sintaxa: %s <adresa_server> <port>\n", argv[0]);
    return -1;
  }

  port = atoi(argv[2]);

  if ((sd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
  {
    perror("Eroare la socket().\n");
    return errno;
  }

  server.sin_family = AF_INET;
  server.sin_addr.s_addr = inet_addr(argv[1]);
  server.sin_port = htons(port);

  if (connect(sd, (struct sockaddr *)&server, sizeof(struct sockaddr)) == -1)
  {
    perror("[client]Eroare la connect().\n");
    return errno;
  }

  char username[100];
  char password[100];
  char login[100];
  char rasp[20000];
  int log;

  printf("Hellow \n");
  printf("Logati-va: \n");
  printf("introduce-ti username:\n");
  fflush(stdout);
  read(0, username, sizeof(username));
  printf("introduce-ti parola:\n");
  read(0, password, sizeof(password));
  // printf("%s\n",password);
  memset(login, '\0', 100);
  strcpy(login, username);
  // strcat(login, "\n");
  strcat(login, password);

  if (write(sd, login, strlen(login)) <= 0)
  {
    perror("[client] eroare la write spre server- username");
    return errno;
  }
  // sleep(2);

  if (read(sd, &log, sizeof(int)) < 0)
  {
    perror("[client]Eroare la read() de la server.\n");
    return errno;
  }
  // printf("%d \n", log);

  if (log)
  {
    // strncpy(username, username, strlen(username) -2);
    printf("[client] login cu succes!\n");
    sleep(2);
    printf("Variante dispoinibile: \n");
    printf("1. Afisarea tuturor curselor de tren \n");
    printf("2. Afisarea trenurilor ce pleaca in urmatoarea ora \n");
    printf("3. Afisarea trenurilor ce sosesc in urmatoarea ora \n");
    printf("4. Anuntarea unei intarzieri \n");
    printf("->%s  Introduceti numarul corespunzator optiunii : \n", username);
    fflush(stdout);
    read(0, buf, sizeof(buf));
    nr = atoi(buf);
    char ruta[200];

    printf("[client] Am citit %d\n", nr);

    if (write(sd, &nr, sizeof(int)) <= 0)
    {
      perror("[client]Eroare la write() spre server.\n");
      return errno;
    }

    if (nr == 4)
    {
      printf("introduceti parametrii de forma statie-pleacare statie-sosire timp intarziat(numar) \n");
      read(0, ruta, sizeof(ruta));
      // printf("%s \n", ruta);
      if (write(sd, ruta, strlen(ruta)) <= 0)
      {
        perror("[client]Eroare la write() spre server.\n");
        return errno;
      }
    }
    // if(nr s)
    int xa = 1;
    if (nr == 5)
    {

      if (fork() == 0)
      {
        while (1)
        {
          char sir[200];
          read(sd, sir, 1000);
          printf("\n\n\n\n\n\n\n\n\n\n %s ", sir);
          sleep(5);
        }
      }

      // parent process because return value non-zero.
      else
      {
        while (1)
        {
          read(0, ruta, sizeof(ruta));
          write(sd, ruta, 1000);
          sleep(5);
        }
      }
    }

    char buffer[20000];
    if (read(sd, buffer, 20000) < 0)
    {
      perror("[client]Eroare la read() de la server.\n");
      return errno;
    }

    printf("[client]Mesajul primit este: \n%s\n", buffer);
  }
  else
  {
    printf("login esuat \n");
    printf("va rugam sa va relogati");
  }
  close(sd);
}